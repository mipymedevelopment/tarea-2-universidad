"use strict";
function validateForm() {
    'use strict';
    var forms = document.querySelectorAll('.needs-validation');
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
        form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
                event.preventDefault();
                event.stopPropagation();
            }
            else {
                event.preventDefault(); /* La validacion se hace con expresiones regulares y usando atributos html */
                event.stopPropagation(); /* En caso de que el formulario sea valido, se previene el envio y se muestra el mensaje... */
                showMessage(); /* ... tal y como pide la tarea. En caso de necesitar realemente enviar el formulario... */
                /* ... probablemente usaria fetch para enviar un POST, y al recibir la respuesta del servidor... */
                /* ...ahi mostraria el mensaje */
            }
            form.classList.add('was-validated');
        }, false);
    });
}
/* ----------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/
function showMessage() {
    var main = document.getElementById('main');
    main.innerHTML = "\n\t\t<div id='message'> <h4>Hemos recibido sus datos, pronto nos estaremos comunicando con usted </h4> </div>\n\t";
}
/* ----------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/
function limpiarDatos() {
    var form = document.getElementById('main-form');
    form.reset();
}
/* ----------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/
function handleOtherCourse(element) {
    var divOtherCourse = document.getElementById('other-course');
    if (element.checked) {
        divOtherCourse.innerHTML = "\n\t\t\t\t\t\t\t<div class=\"input-group\">\n\t\t\t\t\t\t\t\t<span class=\"input-group-text\" id=\"addon-other-course\">Otro curso</span>\n\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" placeholder=\"Curso\" aria-label=\"Nombre\" aria-describedby=\"addon-other-course\" required>\n\t\t\t\t\t\t\t\t<div class=\"invalid-feedback\">\n\t\t\t\t\t\t\t\t\tPor favor complete este campo.\t\n\t\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t</div>";
    }
    else {
        divOtherCourse.innerHTML = '';
    }
}
/* ----------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/
validateForm();
