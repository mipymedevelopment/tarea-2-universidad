
function validateForm() {
    'use strict'

	var forms = document.querySelectorAll('.needs-validation')
  
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event:any) {
          if (!form.checkValidity()) {
            event.preventDefault()
        	event.stopPropagation()
          }else{
				event.preventDefault()				/* La validacion se hace con expresiones regulares y usando atributos html */
            	event.stopPropagation()				/* En caso de que el formulario sea valido, se previene el envio y se muestra el mensaje... */
				showMessage()						/* ... asi como lo pide la tarea. En caso de necesitar realemente enviar el formulario... */
													/* ... probablemente usaria fetch para enviar un POST, y al recibir la respuesta del servidor... */
													/* ...ahi mostraria el mensaje */
													
		  }
		  
          form.classList.add('was-validated')
        }, false)
      })
  }

/* ----------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/

function showMessage(){
	let main:any = document.getElementById('main')
	main.innerHTML = `
		<div id='message'> <h4>Hemos recibido sus datos, pronto nos estaremos comunicando con usted </h4> </div>
	`
}

/* ----------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/

function limpiarDatos(){
	let form:any = document.getElementById('main-form')
	form.reset()
}
/* ----------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/

function handleOtherCourse(element:any){
    let divOtherCourse:any = document.getElementById('other-course')
	if(element.checked){
		divOtherCourse.innerHTML = `
							<div class="input-group">
								<span class="input-group-text" id="addon-other-course">Otro curso</span>
								<input type="text" class="form-control" placeholder="Curso" aria-label="Nombre" aria-describedby="addon-other-course" required>
								<div class="invalid-feedback">
									Por favor complete este campo.	
								</div>
							</div>`
    }else{
		divOtherCourse.innerHTML = ''
	}
}
/* ----------------------------------------------------------------------------------------------------*/
/* ----------------------------------------------------------------------------------------------------*/

validateForm()